# Agile Methoden Publishing Platform

Dieses Repository bezieht sich auf die User Story [Publication of App](http://metaproject.in.fhkn.de:8080/browse/AUNA-12), was sich wiederum auf die folgende Frage bezieht:

> Wie kann sichergestellt werden, dass die aus Thema 2 und 3 entstandenen Apps mit dem Testsystem zum Langzeit-Test zur Verfügung stehen?

Dieses Repository betrifft also alle "Frontend"-Teams, und stellt eine zentrale Plattform dar, damit Minor- und Major-Versionen der Apps veröffentlicht werden können. Somit können die Project Owner, sowie auch die anderen Teams, Vorabversionen eurer Apps herunteladen und testen.

Um die Project Owner zufrieden zu stellen, ist es wichtig, dass ihr die neuen .apk-Dateien eurer App in dieses Repository pusht, sobald ihr eine neue lauffähige Version erstellt habt.

## Dieses Repository Abonnieren

Damit ihr auch immer auf dem neuesten Stand seid, und seht, was die anderen Teams machen, könnt ihr das Repository abonnieren. Dies macht ihr mit den folgenden einfachen Schritten:

1. Erstellt euch einen kostenlosen Account bei [bitbucket.org](https://bitbucket.org/), und meldet euch an. 
2. Navigiert zum Repository [Publishing Platform AUME](https://bitbucket.org/todroth/publishing-platform-aume).
3. Klickt auf das Auge-Symbol oben rechts, und dort auf "Watch this Repository" 

![Bitbucket Login](images_readme/01_login.png "Bitbucket Login")
![Watch Repository](images_readme/02_watch.png "Watch Repository")

Nun werdet ihr informiert, sobald sich etwas am Repository tut.

## Neue Versionen zum Repository hinzufügen

Der Umgang mit Git sollte euch Entwicklern geläufig sein, weshalb darauf hier nur grob eingegangen wird:

* Um neue Versionen eurer .apk in das Repository zu pushen, müsst ihr die nötigen Rechte am Repository besitzen. Dazu hat Tobias Droth [im AUME Moodle-Forum einen Thread](https://moodle.htwg-konstanz.de/moodle/mod/forum/discuss.php?d=21539) eröffnet, in welchem ihm mindestens eine Person aus jedem Team die E-Mail-Addresse mitteilen soll, damit er die nötigen Schreibrechte vergeben kann.
* Habt ihr die nötigen Rechte, könnt ihr mit `git clone https://<USERNAME>@bitbucket.org/todroth/publishing-platform-aume.git` eine Kopie des Repositories auf euren PC herunterladen. Bearbeitet als erstes bitte die README.md-Datei im Ordner eures jeweilgen Teams, und erklärt dort kurz, was eure App macht und welche Herausstellungsmerkmale sie bietet.
* Im Anschluss daran navigiert ihr in den Ordner eures jeweilgen Teams, und kopiert die neue Version eurer .apk-Datei dort hinein. Ist bereits eine .apk-Datei im Ordner vorhanden, könnt ihr diese überschreiben.
* Added die Datei mit `git add *` und committet sie mit `git commit -m "<MESSAGE>"`, wobei <MESSAGE> ein aussagekräftiger Changelog dieser Version gegenüber der letzten Version sein soll! Nur so ist es für die Product Owner ersichtlich, was geändert wurde, und worauf sie bei ihren Tests den Blick legen können.
* Fügt das Changelog zu der README-Datei in eurem Team-Ordner ein.
* Jetzt kann die Datei mit `git push -u origin master` in das Repository gepusht werden.

Bitte denkt daran: Gestaltet das Changelog aussagekräftig, und fügt es in die README-Datei in eurem Team-Ordner ein. Nur so erkennt man, was euer Team geleistet hat, und was in der Version hinzugekommen ist.

## Updates herunterladen

Um die neuen Updates der anderen Teams herunterzuladen, genügt es, auf eurem System in den Ordner zu navigieren, in den ihr das Repository zuvor geklont habt, und `git pull` aufzurufen. Nun habt ihr in jedem der Team-Ordner die letzte hochgeladene Version.